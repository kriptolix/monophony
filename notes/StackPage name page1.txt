StackPage {
        name: "page1";
        title: _("Search");
        child: 
        Box {
          margin-top: 20;
          margin-bottom: 20;
          orientation: vertical;

          SearchBar {
            search-mode-enabled: true;
            halign: center;
            hexpand: true;

            SearchEntry {
            }
          }

          Adw.PreferencesPage {
            use-underline: true;
            title: "Results";

            Adw.PreferencesGroup {
              title: "Top Results";

              Adw.PreferencesRow {
                Box {
                  orientation: horizontal;

                  Button {
                    icon-name: "media-playback-start-symbolic";
                    halign: start;
                  }

                  Box {
                    orientation: vertical;
                    hexpand: true;

                    Label {
                      label: "Music Name";
                    }

                    Label {
                      label: "00:00";
                    }
                  }

                  Button {
                    halign: end;
                    icon-name: "view-more-symbolic";
                  }
                }
              }
            }
          }
        }

        ;
      }
