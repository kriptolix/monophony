<?xml version="1.0" encoding="UTF-8"?>
<interface>
  <requires lib="gtk" version="4.0"/>
  <object class="GtkWindow" id="Main">
    <child type="titlebar">
      <object class="AdwHeaderBar">
        <property name="decoration-layout">icon:close</property>
        <child type="title">
          <object class="GtkStackSwitcher">
            <property name="stack">stack</property>
          </object>
        </child>
      </object>
    </child>
    <child>
      <object class="GtkBox">
        <property name="orientation">1</property>
        <child>
          <object class="GtkStack" id="stack">
            <property name="transition-type">1</property>
            <child>
              <object class="GtkStackPage">
                <property name="name">page1</property>
                <property name="title" translatable="true">Search</property>
                <property name="child">
                  <object class="GtkBox">
                    <property name="margin-top">20</property>
                    <property name="margin-bottom">20</property>
                    <property name="orientation">1</property>
                    <child>
                      <object class="GtkSearchBar">
                        <property name="search-mode-enabled">true</property>
                        <property name="halign">3</property>
                        <property name="hexpand">true</property>
                        <child>
                          <object class="GtkSearchEntry"></object>
                        </child>
                      </object>
                    </child>
                    <child>
                      <object class="AdwPreferencesPage">
                        <property name="use-underline">true</property>
                        <property name="title">Results</property>
                        <child>
                          <object class="AdwPreferencesGroup">
                            <property name="title">Top Results</property>
                            <child>
                              <object class="AdwPreferencesRow">
                                <child>
                                  <object class="GtkBox">
                                    <property name="orientation">0</property>
                                    <child>
                                      <object class="GtkButton">
                                        <property name="icon-name">media-playback-start-symbolic</property>
                                        <property name="halign">1</property>
                                      </object>
                                    </child>
                                    <child>
                                      <object class="GtkBox">
                                        <property name="orientation">1</property>
                                        <property name="hexpand">true</property>
                                        <child>
                                          <object class="GtkLabel">
                                            <property name="label">Music Name</property>
                                          </object>
                                        </child>
                                        <child>
                                          <object class="GtkLabel">
                                            <property name="label">00:00</property>
                                          </object>
                                        </child>
                                      </object>
                                    </child>
                                    <child>
                                      <object class="GtkButton">
                                        <property name="halign">2</property>
                                        <property name="icon-name">view-more-symbolic</property>
                                      </object>
                                    </child>
                                  </object>
                                </child>
                              </object>
                            </child>
                          </object>
                        </child>
                      </object>
                    </child>
                  </object>
                </property>
              </object>
            </child>
            <child>
              <object class="GtkStackPage">
                <property name="name">page2</property>
                <property name="title" translatable="true">Library</property>
                <property name="child">
                  <object class="GtkBox">
                    <child>
                      <object class="AdwPreferencesPage">
                        <property name="use-underline">true</property>
                        <property name="title">Results</property>
                        <child>
                          <object class="AdwPreferencesGroup">
                            <property name="title">Yor Playlists</property>
                            <child>
                              <object class="AdwPreferencesRow">
                                <child>
                                  <object class="GtkBox">
                                    <property name="orientation">0</property>
                                    <child>
                                      <object class="GtkButton">
                                        <property name="icon-name">media-playback-start-symbolic</property>
                                        <property name="halign">1</property>
                                      </object>
                                    </child>
                                    <child>
                                      <object class="GtkBox">
                                        <property name="orientation">1</property>
                                        <property name="hexpand">true</property>
                                        <child>
                                          <object class="GtkLabel">
                                            <property name="label">Playlist Name</property>
                                          </object>
                                        </child>
                                      </object>
                                    </child>
                                    <child>
                                      <object class="GtkButton">
                                        <property name="halign">2</property>
                                        <property name="icon-name">view-more-symbolic</property>
                                      </object>
                                    </child>
                                  </object>
                                </child>
                              </object>
                            </child>
                          </object>
                        </child>
                      </object>
                    </child>
                  </object>
                </property>
              </object>
            </child>
          </object>
        </child>
      </object>
    </child>
  </object>
</interface>
